(function () {

	const locale = {
		progressTooltip: '${{value}} still needed for this project',
		daysLeftMessage: '**Only {{value}} days left** to fund this project',
		mainMessage: 'Join the **{{value}}** other donors who have already supported this project. Every dollar helps.',
		donateButton: 'Give Now',
		whyGiveLink: 'Why give ${{value}}?',
		saveLaterButton: 'Save for Later',
		tellFriendsButton: 'Tell your friends',
	};

	const mockData = {
		daysLeft: 3,
		donorsCount: 42,
		defaultDonation: 50,
		neededDonation: 167,
		totalDonation: 300,
	};

	const jWidget = $('.donation-widget');
	const jProgressBar = jWidget.find('.progress-bar');
	const jProgressBarValue = jWidget.find('.progress-bar-value');
	const jProgressTooltip = jWidget.find('.progress-tooltip');
	const jProgressTooltipText = jWidget.find('.progress-tooltip-text');
	const jProgressTooltipArrow = jWidget.find('.progress-tooltip-arrow');
	const jDaysLeft = jWidget.find('.days-left');
	const jMainMessage = jWidget.find('.main-message');
	const jDonationInput = jWidget.find('.donation-input');
	const jDonateButton = jWidget.find('.donate-button');
	const jWhyGiveLink = jWidget.find('.why-give-link');
	const jSaveLaterButton = jWidget.find('.save-later-button');
	const jTellFriendsButton = jWidget.find('.tell-friends-button');
	let currentDonation = mockData.defaultDonation;


	function init() {
		jDaysLeft.html(
			getHtmlFromMarkdown(locale.daysLeftMessage)
				.replace('{{value}}', mockData.daysLeft),
		);

		jMainMessage.html(
			getHtmlFromMarkdown(locale.mainMessage)
				.replace('{{value}}', mockData.donorsCount),
		);

		jDonationInput.val(currentDonation);
		jDonateButton.html(locale.donateButton);
		jSaveLaterButton.html(locale.saveLaterButton);
		jTellFriendsButton.html(locale.tellFriendsButton);

		renderDonation();
		renderProgressBar();
		attachEvents();
	}


	function attachEvents() {
		jDonationInput.on('change', onChangeDonation);
		jDonateButton.on('click', onClickDonate);
		jSaveLaterButton.on('click', onClickSaveLater);
		jTellFriendsButton.on('click', onClickTellFriends);
	}


	function getHtmlFromMarkdown(value) {
		let htmlValue = value;

		while (htmlValue.includes('**')) {
			htmlValue = htmlValue.replace('**', '<b>');
			htmlValue = htmlValue.replace('**', '</b>');
		}

		return htmlValue;
	}


	function onChangeDonation() {
		currentDonation = parseInt(jDonationInput.val());
		renderDonation();
	}


	function renderDonation() {
		jWhyGiveLink.html(
			getHtmlFromMarkdown(locale.whyGiveLink)
				.replace('{{value}}', currentDonation),
		);
	}


	function onClickDonate() {
		mockData.neededDonation -= currentDonation;
		if (mockData.neededDonation < 0) {
			mockData.neededDonation = 0;
		}

		mockData.donorsCount++;
		jMainMessage.html(
			getHtmlFromMarkdown(locale.mainMessage)
				.replace('{{value}}', mockData.donorsCount),
		);

		renderProgressBar();

		alert('Donation received: $' + currentDonation);
	}


	function onClickSaveLater() {
		alert('Saved for later.');
	}


	function onClickTellFriends() {
		alert('Your friends will know.');
	}


	function renderProgressBar() {
		const progress = (mockData.totalDonation - mockData.neededDonation) / mockData.totalDonation;

		jProgressBar.attr('aria-valuenow', progress * 100);
		jProgressBarValue.css('width', progress * 100 + '%');
		jProgressTooltipArrow.css('right', ((1 - progress) / 2) * 100 + '%');

		jProgressTooltipText.html(
			getHtmlFromMarkdown(locale.progressTooltip)
				.replace('{{value}}', mockData.neededDonation),
		);

		if (mockData.neededDonation <= 0) {
			jProgressTooltip.addClass('disabled');
		}
	}


	init();

})();
