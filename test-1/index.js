(function () {

	const jNewsWidget = $('.news-widget');
	const jPageButtons = jNewsWidget.find('.page-button');
	const jNewsList = jNewsWidget.find('.news-list');
	const jNewsItemTemplate = jNewsWidget.find('.news-item');
	jNewsItemTemplate.detach();
	let newsItems;
	let currentPage;
	let newsPerPage;
	const numTotalPages = 3;


	function init() {
		fetch('http://www.mocky.io/v2/58fda6ce0f0000c40908b8c8')
			.then((resp) => resp.json())
			.then((resp) => {
				newsItems = resp.news;
				newsPerPage = parseInt(newsItems.length / numTotalPages);
				currentPage = 1;

				renderNewsItems();
				initPagination();
			});
	}


	function initPagination() {
		jPageButtons.each((index, elem) => {
			$(elem).on('click', () => onClickPageButton(index + 1));
		});

		setInterval(autoNextPage, 15 * 1000);

		setTimeout(() => location.reload(), 3 * 60 * 1000);
	}


	function renderNewsItems() {
		const start = (currentPage - 1) * newsPerPage;
		const end = (currentPage) * newsPerPage;

		jNewsList.empty();

		for (let i = start; i < end; i++) {
			const item = newsItems[i];
			const jNewsItem = jNewsItemTemplate.clone();

			jNewsItem.find('.item-title').html(item.title);
			jNewsItem.find('.item-details').html(item.details);

			jNewsList.append(jNewsItem);
		}

		jPageButtons.removeClass('active');
		$(jPageButtons[currentPage - 1]).addClass('active');
	}


	function onClickPageButton(pageNumber) {
		currentPage = pageNumber;
		renderNewsItems();
	}


	function autoNextPage() {
		currentPage = currentPage % numTotalPages + 1;
		renderNewsItems();
	}


	init();

})();
